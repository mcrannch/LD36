﻿//Twitch Mareske
//Discord Maer

using UnityEngine;
using System.Collections.Generic;

public class NameManager : MonoBehaviour {

    public List<string> nameList = new List<string>();
    public static NameManager me;

    void Awake()
    {
        me = this;
    }

    public string GetRandomName()
    {
        return nameList[Random.Range(0, nameList.Count)];
    }
}
