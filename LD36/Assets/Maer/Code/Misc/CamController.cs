﻿//Twitch Mareske
//Discord Maer

using System.Collections;
using UnityEngine;

public class CamController : MonoBehaviour {
    [Header("Movement")]
    public float camSpeed = 5;

    public int maxZoomIn = 5;
    public int maxZoomOut = 30;
    [Header("Zoom")]
    public float zoomSpeed = 5;
    private Transform myTrans;

    void CamMovement() {
        myTrans.Translate(Input.GetAxis("Horizontal") * (camSpeed * Time.deltaTime), 0, Input.GetAxis("Vertical") * (camSpeed * Time.deltaTime));
    }

    void CamZoom() {
        //Zooming out
        if(Input.GetKey(KeyCode.Q)) {
            myTrans.Translate(0, -1 * (zoomSpeed * Time.deltaTime), 0);
        }

        if(Input.GetKey(KeyCode.E)) {
            myTrans.Translate(0, +1 * (zoomSpeed * Time.deltaTime), 0);
        }

        if(myTrans.position.y < maxZoomIn)
            myTrans.position = new Vector3(myTrans.position.x, maxZoomIn, myTrans.position.z);

        if(myTrans.position.y > maxZoomOut)
            myTrans.position = new Vector3(myTrans.position.x, maxZoomOut, myTrans.position.z);
    }

    void Start() {
        myTrans = this.transform;
        FunManager.AddChats();
    }

    // Update is called once per frame
    void Update() {
        CamMovement();
        CamZoom();
    }
}