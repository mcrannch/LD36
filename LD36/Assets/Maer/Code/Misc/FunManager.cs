﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class FunManager : MonoBehaviour {

    public static List<string> chatting = new List<string>();
    public static GameObject lastChatter;

    public static List<GameObject> chatter = new List<GameObject>();

    public static void AddChats()
    {
        chatting.Add("Did you heard of Ludum dare?");
        chatting.Add("Nope, what is this?");
        chatting.Add("A game jam, where HD is working on a Game.");
        chatting.Add("Good to know.");
        chatting.Add("Did you know that the bees are dying out?");
        chatting.Add("Nope, but why should i care?");
        chatting.Add("Because if the bees die, most of our current food dies with them.");
        chatting.Add("That is terrible, we need to save them");
        chatting.Add("Yeah, save the bees.");
        chatting.Add("SAVE THE BEES! SAVE THE BEES!");
        chatting.Add("SAVE THE BEES! SAVE THE BEES!");
        chatting.Add("SAVE THE BEES! SAVE THE BEES!");
        chatting.Add("SAVE THE BEES! SAVE THE BEES!");
    }

    public static string GetChat(GameObject _chatter)
    {
        if(lastChatter != _chatter)
        {
            if (chatter.Count > 1)
            {
                if (chatting.Count != 0)
                {
                    string tempString = chatting[0];
                    lastChatter = _chatter;
                    chatting.RemoveAt(0);
                    return tempString;
                }
            }
        }

        return null;

    }

    public static void AddChatter(GameObject _chatter)
    {
        chatter.Add(_chatter);
    }

    public static void RemoveChatter(GameObject _chatter)
    {
        chatter.Remove(_chatter);
    }
}
