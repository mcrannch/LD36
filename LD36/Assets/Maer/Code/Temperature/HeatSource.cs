﻿//Twitch Mareske
//Discord Maer

using System.Collections;
using UnityEngine;

public class HeatSource : MonoBehaviour {
    public float heatGeneration = 20;
    public float heatRange = 10;

    public void OnDisable() {
        TemperatureManager.me.RemoveHeatSource(this);
    }

    void Start() {
        TemperatureManager.me.AddHeatSource(this);
    }
}