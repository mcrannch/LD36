﻿using UnityEngine;
using System.Collections;

public class LoadSettings : MonoBehaviour
{
    private GraphicsOptions graphicsOptions;
    
    void Awake()
    {
        graphicsOptions = GameObject.Find("OptionsCanvas").GetComponent<GraphicsOptions>();
    }

    void Start()
    {
        graphicsOptions.FirstLoad();
        graphicsOptions.UpdateSettings();
    }
}