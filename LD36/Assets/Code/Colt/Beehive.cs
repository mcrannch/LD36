﻿using System.Collections;
using UnityEngine;

public class Beehive : MonoBehaviour {

    public void Fall() {
        transform.SetParent(null);//GetEnvironmentInteractableBase(transform.parent));
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.useGravity = true;
    }

    Transform GetEnvironmentInteractableBase(Transform trans) {
        if(trans == null || trans.name == "EnvironmentInteractables")
            return trans;
        return GetEnvironmentInteractableBase(trans.parent);
    }
}