﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MouseController : MonoBehaviour {
    public GameObject buttons;
    EventSystem eventSystem;
    EnvironmentController selectedEnvironment;
    PersonController selectedPerson;

    public void Start() {
        eventSystem = EventSystem.current;
    }

    #region Controller

    public void OnClick(int buttonId) {
        selectedPerson.InteractWith(selectedEnvironment, buttonId);
    }

    void ShowActionUI() {
        var showButton1 = selectedPerson != null && selectedEnvironment != null;
        buttons.transform.GetChild(0).gameObject.SetActive(showButton1);
    }

    void Update() {
        if(Input.GetMouseButtonDown(0)) {
            if(eventSystem.currentSelectedGameObject == null) {
                selectedPerson = Select(people);
            }
            ShowActionUI();
        } else if(Input.GetMouseButtonDown(1)) {
            selectedEnvironment = Select(environment);
            ShowActionUI();
        }
    }

    #endregion Controller

    #region Shared

    private static void Select<T>(T controller, List<T> entities) where T : EntityController {
        for(int i = 0; i < entities.Count; i++) {
            entities[i].selectedUI.SetActive(entities[i] == controller);
        }
    }

    private static T Select<T>(List<T> entities) where T : EntityController {
        RaycastHit hit;
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out hit)) {
            var controller = hit.transform.gameObject.GetComponent<T>();
            Select(controller, entities);
            return controller;
        } else {
            Select(null, entities);
            return null;
        }
    }

    #endregion Shared

    #region People
    public static List<PersonController> people
            = new List<PersonController>();

    #endregion People

    #region Environment

    public static List<EnvironmentController> environment
        = new List<EnvironmentController>();

    #endregion Environment
}