﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChatBubbleManager : MonoBehaviour
{
    private static ChatBubbleManager instance;

    public GameObject chatBubblePrefab;
    public RectTransform bubbleContainer;

    public List<ChatBubble> bubbles = new List<ChatBubble>();

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            return;
        }

        instance = this;
    }

    /// <summary>
    /// Creates a chat bubble in the ui layer over the target object
    /// </summary>
    /// <param name="target">target object for chat bubble</param>
    /// <param name="text">text in the chat bubble</param>
    /// <param name="lifetime">time that the bubble will be displayed before being destroyed</param>
    /// <param name="offset">screen position offset of chat bubble</param>
    /// <param name="color">color of the text in the bubble</param>
    public static void DisplayChatBubble(Transform target, string text, float lifetime, Vector3 offset, Color color)
    {
        if (instance == null) { Debug.LogError("No instance of chat bubble manager found."); return; }

        var go = GameObject.Instantiate(instance.chatBubblePrefab, instance.bubbleContainer) as GameObject;

        var bubble = go.GetComponent<ChatBubble>();

        if (bubble == null)
        {
            GameObject.Destroy(go);
            return;
        }

        instance.bubbles.Add(bubble);
        bubble.label.text = text;
        bubble.label.color = color;
        bubble.target = target;
        bubble.positionModifier = offset;
        bubble.lifetime = lifetime;

        OrderBubbles(target);

        if (lifetime > 0)
        {
            bubble.StartFinalCountdown();
        }
    }

    public static void RemoveBubble(ChatBubble bubble)
    {
        if (instance.bubbles.Contains(bubble))
        {
            instance.bubbles.Remove(bubble);

            OrderBubbles(bubble.target);
        }
    }

    private static void OrderBubbles(Transform target)
    {
        var allBubblesForTarget = (from b in instance.bubbles where b.target == target select b).OrderByDescending(t => t.lifetime).ToList();
        if (allBubblesForTarget.Count > 1)
        {
            for (int i = 0; i < allBubblesForTarget.Count; i++)
            {
                allBubblesForTarget[i].positionModifier = new Vector3(0, i * 30, 0);
            }
        }
    }
}