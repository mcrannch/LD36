﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampfireFueled : Ressource {
    public GameObject fireEffect;
    public float initialFuel = 60;

    /// <summary>
    /// amount of fuel burned in a second
    /// </summary>
    public float rateOfBurn = 1;

    /// <summary>
    /// current fuel objects in the fire
    /// </summary>
    private List<Fuel> fuelOnFire = new List<Fuel>();

    /// <summary>
    /// actual fuel
    /// </summary>
    private float totalFuel;

    public void AddFuel(Fuel _fuel) {
        fuelOnFire.Add(_fuel);
    }

    public void StartTheFire() {
        fireEffect.SetActive(true);
        totalFuel = initialFuel;
        StartCoroutine(Burn());
    }

    protected override void HarvestActionComplete(BasicWorker worker) {
        Fuel fuel;
        if(worker.inventory.TryRemoveFirstItemOfType<Fuel>(out fuel)) {
            print("fuel added to the fire");
            AddFuel(fuel);
        }

        HarvestCompleteNoLoot();
    }

    private IEnumerator Burn() {
        while(totalFuel > 0) {
            for(int i = fuelOnFire.Count - 1; i >= 0; i--) {
                totalFuel += fuelOnFire[i].Consume();
                if(fuelOnFire[i].IsConsumed)
                    fuelOnFire.Remove(fuelOnFire[i]);
            }

            totalFuel -= rateOfBurn;

            yield return new WaitForSeconds(1);
        }

        Debug.LogError("GG, out of fuel in the campfire");
    }
}