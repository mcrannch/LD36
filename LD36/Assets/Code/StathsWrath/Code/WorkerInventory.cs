﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WorkerInventory : MonoBehaviour
{
    private int CurrentInventoryWeight { get; set; }

    private List<IInventoryItem> items = new List<IInventoryItem>();

    public bool IsEmpty { get { return items.Count == 0; } }

    public int inventorySize;

    public bool AddItem(IInventoryItem item)
    {
        if (CurrentInventoryWeight + item.Weight > inventorySize) return false;

        CurrentInventoryWeight += item.Weight;
        items.Add(item);
        return true;
    }

    public bool TryRemoveFirstItemOfType<T>(out T output) where T : class, IInventoryItem<T>
    {
        var item = (from i in items where i is IInventoryItem<T> select i).FirstOrDefault();

        if (item == null)
        {
            output = null;
            return false;
        }

        items.Remove(item);
        CurrentInventoryWeight -= item.Weight;

        output = (item as IInventoryItem<T>).Item;
        return true;
    }

    public bool HasItemOfType<T>() where T : class, IInventoryItem<T>
    {
        var item = (from i in items where i is IInventoryItem<T> select i).FirstOrDefault();

        return item != null;
    }

    public IInventoryItem GrabItem(string name)
    {
        var item = (from i in items where i.Name == name select i).FirstOrDefault();

        if (item == null) return null;

        items.Remove(item);
        CurrentInventoryWeight -= item.Weight;

        return item;
    }

    public List<string> CheckItems()
    {
        return (from i in items where i != null select i.Name).ToList();
    }
}