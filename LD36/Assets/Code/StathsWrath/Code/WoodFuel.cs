﻿using UnityEngine;

public class WoodFuel : Ressource {
    [Header("Fuel")]
    public Fuel myFuel;

    public override void Awake() {
        base.Awake();

        if(myFuel != null)
            myFuel = Instantiate<Fuel>(myFuel);
        else
            myFuel = ScriptableObject.CreateInstance<Fuel>();
    }

    public void ThrowOnFire(CampfireFueled _fire) {
        _fire.AddFuel(myFuel);
        Destroy(gameObject);
    }

    internal override void StartHarvestEffects() {
        base.StartHarvestEffects();
    }

    internal override void StopHarvestEffects() {
        base.StopHarvestEffects();
    }

    protected override void HarvestActionComplete(BasicWorker worker) {
        Debug.Assert(worker != null);
        Debug.Assert(worker.inventory != null);
        base.HarvestActionComplete(worker);
        worker.inventory.AddItem(myFuel);
        print("harvested log");
        HarvestCompleteNoLoot();
    }

    private void OnDisable() {
        TimeManager.onSeasonChanged -= TimeManager_seasonChanged;
    }

    private void OnEnable() {
        TimeManager.onSeasonChanged += TimeManager_seasonChanged;
    }

    private void TimeManager_seasonChanged(TimeOfDay _dayTime, Season _season) {
        var weatherMod = 1f;

        switch(_season) {
        case Season.Spring:
            weatherMod = .9f;
            break;

        case Season.Summer:
            weatherMod = 1f;
            break;

        case Season.Autumn:
            weatherMod = .85f;
            break;

        case Season.Winter:
            weatherMod = .6f;
            break;

        default:
            break;
        }

        switch(_dayTime) {
        case TimeOfDay.Morning:
            weatherMod += .15f;
            break;

        case TimeOfDay.Midday:
            weatherMod += .25f;
            break;

        case TimeOfDay.Evening:
            weatherMod += -.1f;
            break;

        case TimeOfDay.Night:
            weatherMod += -.2f;
            break;

        default:
            break;
        }

        myFuel.dryingMod = weatherMod;
    }

    private void Update() {
        myFuel.ApplyEnvironmentFactor();
    }
}