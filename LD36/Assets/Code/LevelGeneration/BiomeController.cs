﻿using UnityEngine;
using System.Collections;

public class BiomeController : MonoBehaviour {
    public GameObject baseMesh;
    public float tileHeightOffset = 0;
    public float objectHeightOffset = 0;
    public GameObject[] objectsToSpawn;
    public Vector2[] chanceRange;

    static Vector3 computeCoordinate(int gridX, int gridY, float scaleX, float scaleY, float tileHeightOffset)
    {
        return new Vector3(gridX * scaleX, tileHeightOffset, gridY * scaleY);
    }

    public GameObject GenerateTile(int seed, int x, int y)
    {
        GameObject baseReturn = Instantiate(baseMesh);
        var meshFilter = baseReturn.GetComponent<MeshFilter>();
        var mesh = new Mesh();

        int baseX = x*6;
        int baseY = y;
        if ((y & 1) == 1)
        {
            baseX += 3;
        }

        float scaleX = 2.0f / 6.0f;
        float scaleY = 0.6f;

        Vector3[] vertices = new Vector3[6];
        vertices[0] = computeCoordinate(baseX - 2, baseY + 0, scaleX, scaleY, tileHeightOffset);
        vertices[1] = computeCoordinate(baseX - 1, baseY - 1, scaleX, scaleY, tileHeightOffset);
        vertices[2] = computeCoordinate(baseX + 1, baseY - 1, scaleX, scaleY, tileHeightOffset);
        vertices[3] = computeCoordinate(baseX + 2, baseY + 0, scaleX, scaleY, tileHeightOffset);
        vertices[4] = computeCoordinate(baseX + 1, baseY + 1, scaleX, scaleY, tileHeightOffset);
        vertices[5] = computeCoordinate(baseX - 1, baseY + 1, scaleX, scaleY, tileHeightOffset);
        mesh.vertices = vertices;
        int[] tri = new int[4*3];

        tri[0] = 0;
        tri[1] = 5;
        tri[2] = 1;

        tri[3] = 1;
        tri[4] = 5;
        tri[5] = 2;

        tri[6] = 5;
        tri[7] = 4;
        tri[8] = 2;

        tri[9] = 2;
        tri[10] = 4;
        tri[11] = 3;

        mesh.triangles = tri;

        Vector3[] normals = new Vector3[6];

        normals[0] = Vector3.up;
        normals[1] = Vector3.up;
        normals[2] = Vector3.up;
        normals[3] = Vector3.up;
        normals[4] = Vector3.up;
        normals[5] = Vector3.up;

        mesh.normals = normals;

        Vector2[] uv = new Vector2[6];
        for(int i = 0; i < uv.Length; ++i)
            uv[i] = new Vector2(vertices[i].x*0.1f, vertices[i].z*0.1f);
        mesh.uv = uv;
        meshFilter.mesh = mesh;

        Vector3 translation = computeCoordinate(baseX, baseY, scaleX, scaleY, tileHeightOffset);


        //baseReturn.transform.position = new Vector3(x, tileHeightOffset, y);

        baseReturn.transform.position = new Vector3(0,0,0);
        baseReturn.transform.rotation = Quaternion.identity;
        for (int i = 0; i < objectsToSpawn.Length; i++)
        {
            Random.seed = seed * x * y;
            float rangeValue = Random.Range(0f, 1f);

            if (rangeValue > chanceRange[i].x && rangeValue < chanceRange[i].y)
            {
                GameObject child = Instantiate(objectsToSpawn[i]);
                child.transform.parent = baseReturn.transform;
                child.transform.localPosition = new Vector3(Random.Range(-.5f, .5f), objectHeightOffset, Random.Range(-.5f, .5f)) + translation;
                child.transform.eulerAngles = new Vector3(child.transform.eulerAngles.x, Random.Range(-180, 180), child.transform.eulerAngles.z);
                break;
            }
        }

        return baseReturn;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
