﻿using System;

internal class TemperatureManagerHelpers {

    public static HeatSource GetStrongestHeatSource() {
        var heatSources = TemperatureManager.me.heatSources;
        HeatSource strongest = null;
        for(int i = 0; i < heatSources.Count; i++) {
            if(strongest == null || strongest.heatGeneration * strongest.heatRange
                < heatSources[i].heatGeneration * heatSources[i].heatRange) {
                strongest = heatSources[i];
            }
        }

        return strongest;
    }
}